Q.SPRITE_ENEMY = 4;
Q.animations('enemy', {
    run_normal: { frames: [0,1], loop:true, rate: 1/2}
});

Q.animations('abelha', {
    run_normal: { frames: [0,1], loop:true, rate: 1/4}
});

//enemy that goes up and down
Q.Sprite.extend("Inimigo", {
    init: function(p) {
        this._super(p, {
            vx: -100,
            defaultDirection: "left",
            type: Q.SPRITE_ENEMY,
            collisionMask: Q.SPRITE_DEFAULT

        });
        this.add("2d, aiBounce, animation");

        this.on("bump.right",function(collision) {
            if(collision.obj.isA("Player")) {
                collision.obj.p.strength -= 1;
                if(collision.obj.p.strength <= 0){
                    reiniciarNivel();
                } else {
                    collision.obj.p.vx = +1000;
                    collision.obj.p.vy = -500;
                    Q.stageScene('hud', 3, collision.obj.p);
                }
            }
        });
        this.on("bump.left",function(collision) {
            if(collision.obj.isA("Player")) {
                collision.obj.p.strength -= 1;
                if(collision.obj.p.strength <= 0){
                    reiniciarNivel();
                } else {
                    collision.obj.p.vx = -1000;
                    collision.obj.p.vy = -500;
                    Q.stageScene('hud', 3, collision.obj.p);
                }
            }
        });
        this.on("bump.top",function(collision) {
            if(collision.obj.isA("Player")) {
                //make the player jump
                collision.obj.p.vy = -300;
                Q.audio.play('inimigo.mp3');
                //kill enemy
                this.destroy();
            }
        });
    },
    step: function(dt) {
        this.play("run_normal");
        if(this.p.maxX >= this.p.x){
            this.p.direction = "left";
        }
    }
});

Q.Inimigo.extend("Libelula", {
    init: function(p) {
        this._super(p, {
            sheet: "enemy", sprite: "enemy"
        });

    }
});
Q.Inimigo.extend("Abelha", {
    init: function(p) {
        this._super(p, {
            sheet: "abelha", sprite: "abelha"
        });

    },
    step: function(dt) {
        this.play("run_normal");

    }
});
function adicionarInimigoLibelula(lista, stage){
    for(var i = 0; i < lista.length; i++){
        stage.insert(new Q.Libelula({x: (lista[i][0] * 70), y: (lista[i][1] * 70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    }
}

function adicionarInimigoAbelha(lista, stage){
    for(var i = 0; i < lista.length; i++){
        stage.insert(new Q.Abelha({x: (lista[i][0] * 70), y: (lista[i][1] * 70), rangeY: 70, sheet: "abelha", sprite: "abelha", gravity:0, maxX: (70*20)}));
    }
}