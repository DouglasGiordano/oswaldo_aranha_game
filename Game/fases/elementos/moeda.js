Q.animations('moeda', {
    run_normal: { frames: [0,1], loop:true, rate: 1/2}
});

Q.Sprite.extend("Coins", {
    init: function(p) {
        this._super(p, {
            sheet: "moeda",
            sprite: "moeda",
            scale: 1,
            gravity: 0,
            type: Q.SPRITE_COLLECTABLE,
            collisionMask: Q.SPRITE_PLAYER,
            sensor: true
        });
        this.add("2d, animation");  // Add components
        this.play("run_normal");
        this.on("hit");
    },

    hit: function(col){
        if(col.obj.isA("Player")) {
            Q.audio.play('moeda.mp3');
            this.destroy();
            col.obj.p.moeda += 1;
            Q.stageScene('hud', 3, col.obj.p);
        }
    }
});

Q.animations('arquivo', {
    run_normal: { frames: [0,1], loop:true, rate: 1}
});

Q.Sprite.extend("Arquivo", {
    init: function(p) {
        this._super(p, {
            sheet: "arquivo",
            sprite: "arquivo",
            scale: 1,
            gravity: 0,
            type: Q.SPRITE_COLLECTABLE,
            collisionMask: Q.SPRITE_PLAYER,
            sensor: true,
            informacao: ""
        });
        this.add("2d, animation");  // Add components
        this.play("run_normal");
        this.on("hit");
    },

    hit: function(col){
        if(col.obj.isA("Player")) {
            this.destroy();
            Q.stageScene("arquivo",1, { label: this.p.informacao });
            Q.audio.play('pegar_arquivo.mp3');

        }
    }

});


function adicionarMoeda(lista, stage){
    for(var i = 0; i < lista.length; i++){
        var quantidadeX = lista[i][2];
        var quantidadeY = lista[i][3];
        if(quantidadeX > 0){
            for(var j = 1; j < quantidadeX+1; j++){
                stage.insert(new Q.Coins({x: ((lista[i][0]+j)*70), y: (lista[i][1]*70)}));
            }
        } else {
            if(quantidadeY > 0){
                for(var j = 1; j < quantidadeY+1; j++){
                    stage.insert(new Q.Coins({x: (lista[i][0]*70), y: ((lista[i][1]+j)*70)}));
                }
            }
        }


    }
}