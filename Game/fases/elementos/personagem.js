Q.SPRITE_PLAYER = 1;

Q.animations('player', {

    run_right: { frames: [0,1,2],  rate: 1/8},
    stand_left: { frames: [0]},
    stand_right: { frames: [0]},
    jump: { frames: [0], loop:false, rate: 1},
    run_left: { frames: [0,1,2],  rate: 1/8}
});

//player
Q.Sprite.extend("Player",{
    init: function(p) {
        this._super(p, {
            sheet: "player",        // Spritesheet
            sprite: "player",       // Animationsheet
            x: 70,                 // Initial x position of player
            y: 70,                 // Initial y position of
            flip: 'x',
            jumpSpeed: -800,
            speed: 400,
            strength: 3,
            moeda: 0,
            defaultDirection: "left",
            gravity: 2,
            type: Q.SPRITE_PLAYER,
            collisionMask: Q.SPRITE_DEFAULT | Q.SPRITE_COLLECTABLE | Q.SPRITE_DOOR
        });
        this.add("2d, platformerControls, animation");  // Add components
    },
    step: function(dt) {
        if(Q.inputs['up']) {
            this.play("jump",1);
            //Q.audio.play('pulo.mp3');
        } else if(Q.inputs['right']) {
            this.p.speed = 400;
            this.p.flip="x";
            this.play("run_right");
        } else if(Q.inputs['left']) {
            this.p.flip="";
            this.play("run_left");
        } else {
            this.play("stand_" + this.p.direction);
        }

        if(Q.inputs['fire'] && Q.inputs['right']) {
            this.p.speed = 650;
        }
        console.log(Q("viewport").y);
        if(this.p.y > Q.stage().y){
            reiniciarNivel();
        }
    }
});