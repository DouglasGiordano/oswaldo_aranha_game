
Q.scene("level1",function(stage) {
    var background = new Q.TileLayer({ dataAsset: 'level1.tmx', layerIndex: 0, sheet: 'tiles', tileW: 70, tileH: 70, type: Q.SPRITE_NONE });
    stage.insert(new Q.Repeater({ asset: "background.jpg",
        speedX: 0.3,      // Parralax effect
        speedY: 0.3,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(new Q.Repeater({ asset: "nuvens.png",
        speedX: 0.8,      // Parralax effect
        speedY: 0.2,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(background);

    stage.insert(new Q.Porta({x:(94*70), y: (18.5*70), entrar: function(){
        iniciarFase2();
    }}));

    adicionarInimigoLibelula([
     [20,2], [30,2], [95,2], [90,2], [50,2], [60,2], [80,2], [40,2], [75,2], [68,2], [23,15], [73,11]
     ], stage);
    stage.insert(new Q.Arquivo({x:(23*70), y: (15*70), informacao: curiosidades[0]}));
    stage.insert(new Q.Arquivo({x:(73*70), y: (11*70), informacao: curiosidades[1]}));
    adicionarMoeda([[22, 4, 0, 5], [48,20, 10, 0], [3, 16, 4, 0], [85, 4, 5, 0], [66, 4, 0, 10], [92, 8, 5, 0], [69, 3, 0, 10], [30, 2, 4, 0]], stage);

    stage.insert(new Q.Caixa({x:(87*70), y: (7*70)}));
    stage.insert(new Q.CaixaPulo({x:(67*70), y: (14*70), altura: (24*70)}));

    stage.collisionLayer(new Q.TileLayer({ dataAsset: 'level1.tmx', layerIndex:1,  sheet: 'tiles', tileW: 70, tileH: 70 }));

    var player = stage.insert(new Q.Player());
    stage.add("viewport").follow(player,{x: true, y: true},{minX: 0, maxX: background.p.w, minY: 0, maxY: background.p.h});
    stage.insert(new Q.Repeater({ asset: "gramas.png",
        speedX: 0.9,      // Parralax effect
        speedY: 1,      // Parralax effect
        scale: 1.0, repeatY: false }));
});

function iniciarFase1(){
    Q.clearStages();
    Q.stageScene("level1");
    StageActiveName = "level1";
    Q.audio.play('ambiente.mp3',{loop:true});
}



