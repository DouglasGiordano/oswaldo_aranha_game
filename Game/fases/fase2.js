

Q.scene("level2",function(stage) {
    var background = new Q.TileLayer({ dataAsset: 'level2.tmx', layerIndex: 0, sheet: 'tiles', tileW: 70, tileH: 70, type: Q.SPRITE_NONE });
    stage.insert(new Q.Repeater({ asset: "background.jpg",
        speedX: 0.5,      // Parralax effect
        speedY: 0.5,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(new Q.Porta({x:(189*70), y: (18.5*70), entrar: function(){
        iniciarFase3();
    }}));
    stage.insert(new Q.Repeater({ asset: "nuvens.png",
        speedX: 0.8,      // Parralax effect
        speedY: 0.2,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(background);
    stage.insert(new Q.Arquivo({x:(10*70), y: (9*70), informacao: curiosidades[2]}));
    stage.insert(new Q.Arquivo({x:(22*70), y: (4*70), informacao: curiosidades[3]}));

    stage.insert(new Q.CaixaPulo({x:(15*70), y: (14*70), altura: 1200}));
    stage.insert(new Q.CaixaPulo({x:(17*70), y: (6*70), altura: 1200}));
    stage.insert(new Q.CaixaPulo({x:(88*70), y: (18*70), altura: 2000}));
    stage.insert(new Q.CaixaPulo({x:(108*70), y: (18*70), altura: 2000}));
    stage.insert(new Q.CaixaPulo({x:(135*70), y: (10*70), altura: 2000}));
    stage.insert(new Q.Inimigo({x:(51*70), y: (17*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(63*70), y: (17*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(73*70), y: (17*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(81*70), y: (17*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(98*70), y: (3*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(114*70), y: (3*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(140*70), y: (9*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(171*70), y: (15*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    stage.insert(new Q.Inimigo({x:(182*70), y: (14*70), rangeY: 70, sheet: "enemy", sprite: "enemy" }));
    adicionarMoeda([[35, 3, 5, 0], [52,3, 10, 0], [168, 15, 10, 0], [154, 13, 10, 0], [178, 5, 0, 20]], stage);

    stage.insert(new Q.Coins({x:(88*70), y: (17*70)}));
    stage.insert(new Q.Coins({x:(88*70), y: (16*70)}));
    stage.insert(new Q.Coins({x:(88*70), y: (15*70)}));
    stage.insert(new Q.Coins({x:(88*70), y: (14*70)}));
    stage.insert(new Q.Coins({x:(88*70), y: (12*70)}));
    stage.insert(new Q.Coins({x:(88*70), y: (11*70)}));
    stage.insert(new Q.Coins({x:(105*70), y: (18*70)}));
    stage.insert(new Q.Coins({x:(106*70), y: (18*70)}));
    stage.insert(new Q.Coins({x:(107*70), y: (18*70)}));
    stage.insert(new Q.Coins({x:(109*70), y: (18*70)}));
    stage.insert(new Q.Coins({x:(110*70), y: (18*70)}));
    stage.insert(new Q.Coins({x:(111*70), y: (18*70)}));
    stage.collisionLayer(new Q.TileLayer({ dataAsset: 'level2.tmx', layerIndex:1,  sheet: 'tiles', tileW: 70, tileH: 70 }));

    var player = stage.insert(new Q.Player());
    stage.add("viewport").follow(player,{x: true, y: true},{minX: 0, maxX: background.p.w, minY: 0, maxY: background.p.h});
    stage.insert(new Q.Repeater({ asset: "gramas.png",
        speedX: 0.9,      // Parralax effect
        speedY: 1,      // Parralax effect
        scale: 1.0, repeatY: false }));
});

function iniciarFase2() {
    Q.clearStages();
        Q.stageScene('level2');
        StageActiveName = "level2";
    Q.audio.play('ambiente.mp3',{loop:true});
}
