

Q.scene("level3",function(stage) {
    var background = new Q.TileLayer({ dataAsset: 'level3.tmx', layerIndex: 0, sheet: 'tiles', tileW: 70, tileH: 70, type: Q.SPRITE_NONE });
    stage.insert(new Q.Repeater({ asset: "background.jpg",
        speedX: 0.5,      // Parralax effect
        speedY: 0.5,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(new Q.Repeater({ asset: "nuvens.png",
        speedX: 0.8,      // Parralax effect
        speedY: 0.2,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(background);
    stage.collisionLayer(new Q.TileLayer({ dataAsset: 'level3.tmx', layerIndex:1,  sheet: 'tiles', tileW: 70, tileH: 70 }));
    stage.insert(new Q.Porta({x:(6*70), y: (18.5*70), entrar: function(){
        iniciarFase4();
    }}));
    adicionarInimigoLibelula([
        [22,6], [12,6], [27,6], [58,5], [76,4], [87,4], [100,6], [184,8], [139,17], [76,15], [10,16], [52,17]
    ], stage);
    adicionarMoeda([[8, 3, 0, 4], [70,3, 10, 0], [150, 10, 10, 0], [187, 7, 10, 0], [61, 15, 5, 0]], stage);

    stage.insert(new Q.Arquivo({x:(195*70), y: (7*70), informacao: curiosidades[4]}));

    var player = stage.insert(new Q.Player());
    stage.add("viewport").follow(player,{x: true, y: true},{minX: 0, maxX: background.p.w, minY: 0, maxY: background.p.h});
    stage.insert(new Q.Repeater({ asset: "gramas.png",
        speedX: 0.9,      // Parralax effect
        speedY: 1,      // Parralax effect
        scale: 1.0, repeatY: false }));
});

function iniciarFase3() {
    Q.clearStages();
        Q.stageScene('level3');
        StageActiveName = "level3";
    Q.audio.play('ambiente.mp3',{loop:true});
}
