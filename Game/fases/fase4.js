

Q.scene("level4",function(stage) {
    var background = new Q.TileLayer({ dataAsset: 'level4.tmx', layerIndex: 0, sheet: 'tiles2', tileW: 70, tileH: 70, type: Q.SPRITE_NONE });
    stage.insert(new Q.Repeater({ asset: "background.jpg",
        speedX: 0.5,      // Parralax effect
        speedY: 0.5,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(new Q.Repeater({ asset: "nuvens.png",
        speedX: 0.8,      // Parralax effect
        speedY: 0.2,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(new Q.Repeater({ asset: "solo.png",
        speedX: 0.7,      // Parralax effect
        speedY: 1,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(background);
    stage.collisionLayer(new Q.TileLayer({ dataAsset: 'level4.tmx', layerIndex:1,  sheet: 'tiles2', tileW: 70, tileH: 70 }));

    adicionarInimigoAbelha([
        [3,3], [10,3]
    ], stage);
    //adicionarMoeda([[8, 3, 0, 4], [70,3, 10, 0], [150, 10, 10, 0], [187, 7, 10, 0], [61, 15, 5, 0]], stage);
    var player = stage.insert(new Q.Player());
    stage.add("viewport").follow(player,{x: true, y: true},{minX: 0, maxX: background.p.w, minY: 0, maxY: background.p.h});
    stage.insert(new Q.Repeater({ asset: "gramas.png",
        speedX: 0.9,      // Parralax effect
        speedY: 1,      // Parralax effect
        scale: 1.0, repeatY: false }));
});

function iniciarFase4() {
    Q.load(" solo.png, level4.tmx, abelha.png", function () {//,
        Q.sheet("abelha","abelha.png", { tilew: 70, tileh: 70});
        Q.clearStages();
        Q.stageScene('level4');
        StageActiveName = "level4";
    }, {
        progressCallback: function (loaded, total) {
            var element = document.getElementById("progresso");
            element.innerHTML = Math.floor(loaded / total * 100) + "%";
        }
    });


}
