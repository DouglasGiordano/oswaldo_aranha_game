var StageActiveName = "";
Q.SPRITE_PLAYER = 1;
Q.SPRITE_COLLECTABLE = 2;
Q.SPRITE_ENEMY = 4;
Q.SPRITE_DOOR = 8;

var curiosidades = ["Osvaldo Aranha nasceu a 15 de fevereiro de \n 1894, em Alegrete RS",
    "Filho de Euclydes Egydio de Souza Aranha \ne de Luiza Jacques de Freitas Valle",
    "O pai de Oswaldo Aranha foi Deputado Estadual pelo PRR e Intendente de Itaqui",
    "Oswaldo teve dezessete irm�os(17), mas apenas \n doze (12) alcan�aram a idade adulta",
    "Durante a inf�ncia de Oswaldo Aranha a casa da Estancia \nrecebia amigos ilustres da fam�lia como Joaquim Francisco de Assis Brasil",
    "Oswaldo Aranha foi alfabetizado em casa, por sua m�e, em Itaqui",
    ""];


var box;
Q.animations('porta', {
    run: { frames: [0,1], loop:true, rate: 1/2}
});

Q.Sprite.extend("Porta", {
    init: function(p) {
        this._super(p, {
            sheet: "porta",
            sprite: "porta",
            scale: 1,
            sensor: true,
            type: Q.SPRITE_DOOR,
            gravity: 1,
            collisionMask: Q.SPRITE_NONE || Q.SPRITE_DEFAULT
        });
        this.add("animation");
        this.on("hit");

    },

    hit: function(col){
        if(col.obj.isA("Player")) {
            if (Q.inputs['fire']) {
                this.p.entrar();
            }
        }
    },

    step: function(dt){
        this.play("run");
    }
});

Q.Sprite.extend("Caixa", {
    init: function(p) {
        this._super(p, {
            asset: "caixa.png",
            scale: 1,
            vx: 0,
            vy: 0
        });
        this.add("2d");
        this.on("hit");

        this.on("bump.right",function(collision) {
            if(collision.obj.isA("Player")) {
                    this.p.vx -= 3;

            }
        });
        this.on("bump.left",function(collision) {
            if(collision.obj.isA("Player")) {
                this.p.vx += 3;
            }
        });
    },

    hit: function(col){
    },

    step: function(dt){
    }
});

Q.animations('caixa_pulo', {
    run: { frames: [1,0], loop:false, rate: 1/2}
});

Q.Sprite.extend("CaixaPulo", {
    init: function(p) {
        this._super(p, {
            scale: 1,
            sheet: "caixa_pulo",
            sprite: "caixa_pulo",
            altura: 1200
        });
        this.add("2d, animation");
        this.on("hit");
        this.on("bump.top",function(collision) {
            if(collision.obj.isA("Player")) {
                collision.obj.p.vy = -p.altura;

                this.play("run");
                Q.audio.play('caixa_pulo.mp3');
            }
        });
    },

    hit: function(col){
    },

    step: function(dt){
    }
});

Q.scene('hud',function(stage) {
    var container = stage.insert(new Q.UI.Container({
        fill: "gray",
        x: 50,
        y: 20,
        shadow: 10,
        shadowColor: "rgba(0,0,0,0.5)"

    }));

    var label = container.insert(new Q.UI.Text({x:200, y: 20,
        label: "Pontos: " + stage.options.moeda, color: "white" }));

    var strength = container.insert(new Q.UI.Text({x:50, y: 20,
        label: "Vida: " + stage.options.strength + '%', color: "white" }));

    container.fit(20);
});

Q.scene('arquivo',function(stage) {
    box = stage.insert(new Q.UI.Container({
        x: Q.width/2, y: 50, fill: "rgba(0,125,0,0.6)"
    }));
    var label = box.insert(new Q.UI.Text({x:10, y: -10 ,
        label: stage.options.label,size:16, color: "white"}));

    box.fit(20);
});


function reiniciarNivel(){
    Q.clearStages();
    Q.stageScene(StageActiveName);
}


Q.scene("inicio",function(stage) {
    var background = new Q.TileLayer({ dataAsset: 'inicio.tmx', layerIndex: 0, sheet: 'tilesInicio', tileW: 70, tileH: 70, type: Q.SPRITE_NONE });
    stage.insert(new Q.Repeater({ asset: "inicio_background.jpg",
        speedX: 0.3,      // Parralax effect
        speedY: 0.3,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(new Q.Repeater({ asset: "nuvens.png",
        speedX: 0.8,      // Parralax effect
        speedY: 0.2,      // Parralax effect
        scale: 1.0 }));   // Scale to fit height of window
    stage.insert(background);
    stage.insert(new Q.Porta({x:(7*70), y: (14.5*70), entrar: function(){
        iniciarFase1();
    }}));
    stage.insert(new Q.Porta({x:(9*70), y: (14.5*70), entrar: function(){
        iniciarFase2();
    }}));
    stage.insert(new Q.Porta({x:(11*70), y: (14.5*70), entrar: function(){
        iniciarFase3();
    }}));
    stage.insert(new Q.Porta({x:(13*70), y: (14.5*70), entrar: function(){
        iniciarFase4();
    }}));

    /*stage.insert(new Q.Porta({x:(15*70), y: (14.5*70), entrar: function(){
        iniciarFase2();
    }}));
    stage.insert(new Q.Porta({x:(20*70), y: (14.5*70), entrar: function(){
        iniciarFase3();
    }}));**/
    //stage.insert(new Q.Caixa({x:(10*70), y: (14*70)}));
    //stage.insert(new Q.CaixaPulo({x:(6*70), y: (14*70)}));
    stage.collisionLayer(new Q.TileLayer({ dataAsset: 'inicio.tmx', layerIndex:1,  sheet: 'tilesInicio', tileW: 70, tileH: 70 }));
    stage.insert(new Q.Repeater({ asset: "gramas.png",
        speedX: 0.9,      // Parralax effect
        speedY: 1,      // Parralax effect
        scale: 1.0, repeatY: false }));
    var player = stage.insert(new Q.Player());
    stage.add("viewport").follow(player,{x: true, y: true},{minX: 0, maxX: background.p.w, minY: 0, maxY: background.p.h});
});


Q.scene('hud',function(stage) {
    var container = stage.insert(new Q.UI.Container({
        fill: "gray",
        x: 10,
        y: 30,
        shadow: 5,
        shadowColor: "rgba(0,0,0,0.8)",
        opacity: 0.4

    }));

    container.insert(new Q.UI.Button({
        sheet: "moeda",
        x: 300,
        scale: 1,
        y: 20
    }, function() {
    }));
    var label = container.insert(new Q.UI.Text({x:350, y: 20, size: 18,
        label: "" + stage.options.moeda, color: "white" }));

    for(i = 1; i < stage.options.strength+1; i+=1){
        container.insert(new Q.UI.Button({
            asset: 'coracao.png',
            x: 50*i,
            scale: 1,
            y: 20
        }, function() {
        }));
    }


    container.fit(20);
});

//load assets
    Q.load("mapa_inicio.png, player_sheet.png, inicio.tmx, inicio_background.jpg,coracao.png, pulo.mp3, porta.png, " +
        "gramas.png, nuvens.png, caixa.png, caixa_pulo.png, pulo.mp3, moeda.mp3, inimigo.mp3, pegar_arquivo.mp3, caixa_pulo.mp3, arquivo.png, " +
        "tiles_map.png, enemy_sheet.png, moeda_sheet.png, background.jpg, level1.tmx," +
        "level2.tmx, level3.tmx, tiles_map2.png, ambiente.mp3", function () {//, solo.png, level4.tmx, abelha.png
        Q.sheet("tilesInicio", "mapa_inicio.png", {tilew: 70, tileh: 70});
        Q.sheet("player", "player_sheet.png", {tilew: 70, tileh: 70});
        Q.sheet("caixa_pulo", "caixa_pulo.png", {tilew: 70, tileh: 70});
        Q.sheet("tiles","tiles_map.png", { tilew: 70, tileh: 70});
        Q.sheet("enemy","enemy_sheet.png", { tilew: 70, tileh: 70});
        Q.sheet("moeda","moeda_sheet.png", { tilew: 50, tileh: 50});
        Q.sheet("arquivo","arquivo.png", { tilew: 50, tileh: 50});
        Q.sheet("porta", "porta.png", {tilew: 70, tileh: 70});
        Q.sheet("tiles2","tiles_map2.png", { tilew: 70, tileh: 70});
        iniciarFase1();

        Q.stageScene("arquivo",1, { label: "Utilize as teclas X para pular e as setas direcionais do teclado. \n Utilize a tecla Z para fechar uma janela ou acessar uma fase." });
    }, {
        progressCallback: function (loaded, total) {
            var element = document.getElementById("progresso");
            element.innerHTML = Math.floor(loaded / total * 100) + "%";
        }
    });


